<?php

namespace App\Console\Commands;

use App\Services\RabbitMQ\Jobs\Spreadsheet\Consumer;
use Illuminate\Console\Command;

class SpreadSheet extends Command
{
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'command:spreadsheet';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Scheduler for Inserting from Spreadsheet file to Database';

    /**
     * Execute the console command.
     * @return int
     * @throws \ErrorException
     */
    public function handle()
    {
        Consumer::register();
        return 0;
    }
}
