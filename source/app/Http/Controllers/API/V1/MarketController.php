<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Repositories\Market\MarketRepository;
use App\Models\Resources\MarketResource;
use App\Traits\Pagination;
use Illuminate\Http\Request;

class MarketController extends Controller
{
    use Pagination;

    public function list(Request $request)
    {
        $total = 0;
        $page = $this->getPagination(null, $request->get('length', 10), $request->get('start', 0));
        $markets = (new MarketRepository())->search($page->offset, $page->count, $total);

        return json_encode([
            "data" => (new MarketResource($markets))->toArray(),
            "meta" => [
                "offset" => $page->offset,
                "count" => $page->count,
                "total" => $total
            ]
        ]);
    }
}
