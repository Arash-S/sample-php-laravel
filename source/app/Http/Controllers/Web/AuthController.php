<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Entities\User;
use App\Models\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /** @var */
    private $title;
    /** @var UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->title = '';
        $this->userRepository = $userRepository;
    }

    public function signUp()
    {
        $this->title = 'sign up';
        return view('auth.sign_up')->with(['title' => $this->title]);
    }

    public function signIn()
    {
        $this->title = 'sign in';
        return view('auth.sign_in')->with(['title' => $this->title]);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email:rfc,dns|unique:users|max:255',
            'password' => 'required|confirmed'
        ]);

        $user = new User();
        $user->fill($request->only(['name', 'email', 'password']));
        $this->userRepository->create($user);

        return redirect()->route('sign_in');
    }

    public function login(Request $request)
    {
        $credential = $this->validate($request, [
            'email' => 'required|email:rfc,dns|exists:users|max:255',
            'password' => 'required|max:255'
        ]);

        if (auth()->attempt($credential)) {
            $request->session()->regenerate();
            return redirect()->intended('dashboard');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput(['email']);
    }
}
