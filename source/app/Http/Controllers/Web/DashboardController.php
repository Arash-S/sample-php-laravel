<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /** @var string */
    private $title;

    public function __construct()
    {
        $this->title = '';
    }

    public function index()
    {
        $this->title = 'dashboard';
        return view('dashboard.index')->with(['title' => $this->title]);
    }
}
