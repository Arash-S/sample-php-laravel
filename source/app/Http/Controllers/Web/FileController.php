<?php

namespace App\Http\Controllers\Web;

use App\Models\Entities\File;
use App\Models\Repositories\File\FileRepository;
use App\Http\Controllers\Controller;
use App\Services\RabbitMQ\Jobs\Spreadsheet\Producer;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class FileController extends Controller
{
    /** @var string */
    private $title;
    /** @var FileRepository */
    private $fileRepository;

    public function __construct(FileRepository $fileRepository)
    {
        $this->title = '';
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xlsx,xls|max:20000',
        ]);

        $path = $request->file->store('spreadsheet', 'uploads');
        $path = implode('/', [storage_path('uploads'), $path]);
        $hash = hash_file('sha256', $path);

        $entity = $this->fileRepository->getOneByHash($hash);

        if (is_null($entity)) {
            $file = new File();
            $file->fill(['hash' => $hash, 'path' => $path]);
            $this->fileRepository->create($file);

            Producer::make($file->getPath());
        } else {
            Storage::disk('uploads')->delete(implode('/', ['spreadsheet', basename($path)]));
        }

        return redirect()->back();
    }

}
