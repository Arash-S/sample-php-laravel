<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Repositories\Market\MarketRepository;
use Illuminate\Http\Request;

class MarketController extends Controller
{
    /** @var string */
    private $title;
    /** @var MarketRepository */
    private $marketRepository;

    public function __construct(MarketRepository $marketRepository)
    {
        $this->title = 'market';
        $this->marketRepository = $marketRepository;
    }

    public function index(Request $request)
    {
        return view('market.index')->with(['title' => $this->title]);
    }
}
