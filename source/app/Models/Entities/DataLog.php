<?php

namespace App\Models\Entities;

use App\Models\Enums\DataLogActions;

class DataLog extends Entity
{
    /** @var int */
    protected $id;
    /** @var int */
    protected $userId;
    /** @var string */
    protected $table;
    /** @var int */
    protected $reference;
    /** @var string|DataLogActions */
    protected $action;
    /** @var string */
    protected $data;
    /** @var \DateTime */
    protected $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @return int
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param int $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return string|DataLogActions
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string|DataLogActions $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function fill($params)
    {
        isset($params['id']) && $this->setId($params['id']);
        isset($params['user_id']) && $this->setUserId($params['user_id']);
        isset($params['table']) && $this->setTable($params['table']);
        isset($params['reference']) && $this->setReference($params['reference']);
        isset($params['action']) && $this->setAction($params['action']);
        isset($params['data']) && $this->setData($params['data']);
    }
}
