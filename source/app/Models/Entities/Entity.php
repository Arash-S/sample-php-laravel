<?php

namespace App\Models\Entities;

use Illuminate\Support\Str;
use JsonSerializable;

class Entity implements JsonSerializable
{
    /**
     * @description Original values of attributes
     * @var array
     */
    private $original = [];

    /**
     * @description Constructor
     */
    public function __construct()
    {
    }

    /**
     * @param string $name
     * @return void
     */
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            $function = Str::camel('get_' . Str::snake($name));
            return $this->$function();
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $function = Str::camel('set_' . Str::snake($name));
            $this->$function($value);
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return property_exists($this, $name);
    }

    /**
     * @description Make all variables of the object as null
     * @return $this
     */
    public function clear()
    {
        foreach (get_object_vars($this) as $_name => $_value) {
            $this->$_name = null;
        }

        return $this;
    }

    /**
     * @description Fill the model
     * @param array $params
     * @throws \Exception
     */
    public function fill($params)
    {
        throw new \Exception("Fill Method: Not Implemented");
    }

    /**
     * @description Get an Array of current Attributes value
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * @description Store an array of attributes original value
     * @return void
     */
    public function storeOriginal()
    {
        $this->original = $this->toArray();
    }

    /**
     * get an Array of Changed Attributes
     * @return array
     */
    public function changedAttributesName()
    {
        $changedAttributes = [];
        $attributes = $this->toArray();
        foreach ($attributes as $key => $value) {
            if (isset($this->originals[$key])) {
                if ($value != $this->originals[$key] && !((is_array($this->originals[$key]) || is_object($this->originals[$key])))) {
                    $changedAttributes[] = $key;
                }
            }
        }
        return $changedAttributes;
    }

    /**
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * @description Get an Array of Changed Attributes with original values
     * @return array
     */
    public function getChanges()
    {
        $changes = [];

        foreach ($this->changedAttributesName() as $_key) {
            $changes[$_key] = $this->originals[$_key];
        }

        return $changes;
    }

    /**
     * @description Get an Array of Changed Attributes with new values
     * @return array
     */
    public function getDirty()
    {
        $dirty = [];
        $attributes = $this->toArray();

        foreach ($this->changedAttributesName() as $_key) {
            $dirty[$_key] = $attributes[$_key];
        }

        return $dirty;
    }

    /**
     * @description is any attribute changed?
     * @return bool
     */
    public function isDirty()
    {
        return (0 < count($this->changedAttributesName()));
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
