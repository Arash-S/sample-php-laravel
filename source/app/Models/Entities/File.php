<?php

namespace App\Models\Entities;

class File extends Entity
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $hash;
    /** @var string */
    protected $path;
    /** @var \DateTime */
    protected $created_at;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @param array $params
     */
    public function fill($params)
    {
        isset($params['id']) && $this->setId($params['id']);
        isset($params['hash']) && $this->setHash($params['hash']);
        isset($params['path']) && $this->setPath($params['path']);
        isset($params['created_at']) && $this->setCreatedAt($params['created_at']);
    }
}
