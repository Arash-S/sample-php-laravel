<?php

namespace App\Models\Entities;

class Market extends Entity
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $symbolName;
    /** @var string */
    protected $businessName;
    /** @var int */
    protected $count;
    /** @var int */
    protected $value;
    /** @var int */
    protected $turnover;
    /** @var int */
    protected $lastPreviousTransaction;
    /** @var int */
    protected $firstTransaction;
    /** @var int */
    protected $lastTransaction;
    /** @var double */
    protected $lastTransactionChange;
    /** @var float */
    protected $lastTransactionPercent;
    /** @var int */
    protected $finalTransaction;
    /** @var double */
    protected $finalTransactionChange;
    /** @var float */
    protected $finalTransactionPercent;
    /** @var int */
    protected $lowestTransaction;
    /** @var int */
    protected $highestTransaction;
    /** @var \DateTime */
    protected $date;
    /** @var \DateTime */
    protected $createdAt;
    /** @var \DateTime */
    protected $updatedAt;
    /** @var \DateTime */
    protected $deletedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSymbolName()
    {
        return $this->symbolName;
    }

    /**
     * @param string $symbolName
     */
    public function setSymbolName($symbolName)
    {
        $this->symbolName = $symbolName;
    }

    /**
     * @return string
     */
    public function getBusinessName()
    {
        return $this->businessName;
    }

    /**
     * @param string $businessName
     */
    public function setBusinessName($businessName)
    {
        $this->businessName = $businessName;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * @param int $turnover
     */
    public function setTurnover($turnover)
    {
        $this->turnover = $turnover;
    }

    /**
     * @return int
     */
    public function getLastPreviousTransaction()
    {
        return $this->lastPreviousTransaction;
    }

    /**
     * @param int $lastPreviousTransaction
     */
    public function setLastPreviousTransaction($lastPreviousTransaction)
    {
        $this->lastPreviousTransaction = $lastPreviousTransaction;
    }

    /**
     * @return int
     */
    public function getFirstTransaction()
    {
        return $this->firstTransaction;
    }

    /**
     * @param int $firstTransaction
     */
    public function setFirstTransaction($firstTransaction)
    {
        $this->firstTransaction = $firstTransaction;
    }

    /**
     * @return int
     */
    public function getLastTransaction()
    {
        return $this->lastTransaction;
    }

    /**
     * @param int $lastTransaction
     */
    public function setLastTransaction($lastTransaction)
    {
        $this->lastTransaction = $lastTransaction;
    }

    /**
     * @return float
     */
    public function getLastTransactionChange()
    {
        return $this->lastTransactionChange;
    }

    /**
     * @param float $lastTransactionChange
     */
    public function setLastTransactionChange($lastTransactionChange)
    {
        $this->lastTransactionChange = $lastTransactionChange;
    }

    /**
     * @return float
     */
    public function getLastTransactionPercent()
    {
        return $this->lastTransactionPercent;
    }

    /**
     * @param float $lastTransactionPercent
     */
    public function setLastTransactionPercent($lastTransactionPercent)
    {
        $this->lastTransactionPercent = $lastTransactionPercent;
    }

    /**
     * @return int
     */
    public function getFinalTransaction()
    {
        return $this->finalTransaction;
    }

    /**
     * @param int $finalTransaction
     */
    public function setFinalTransaction($finalTransaction)
    {
        $this->finalTransaction = $finalTransaction;
    }

    /**
     * @return float
     */
    public function getFinalTransactionChange()
    {
        return $this->finalTransactionChange;
    }

    /**
     * @param float $finalTransactionChange
     */
    public function setFinalTransactionChange($finalTransactionChange)
    {
        $this->finalTransactionChange = $finalTransactionChange;
    }

    /**
     * @return float
     */
    public function getFinalTransactionPercent()
    {
        return $this->finalTransactionPercent;
    }

    /**
     * @param float $finalTransactionPercent
     */
    public function setFinalTransactionPercent($finalTransactionPercent)
    {
        $this->finalTransactionPercent = $finalTransactionPercent;
    }

    /**
     * @return int
     */
    public function getLowestTransaction()
    {
        return $this->lowestTransaction;
    }

    /**
     * @param int $lowestTransaction
     */
    public function setLowestTransaction($lowestTransaction)
    {
        $this->lowestTransaction = $lowestTransaction;
    }

    /**
     * @return int
     */
    public function getHighestTransaction()
    {
        return $this->highestTransaction;
    }

    /**
     * @param int $highestTransaction
     */
    public function setHighestTransaction($highestTransaction)
    {
        $this->highestTransaction = $highestTransaction;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    public function fill($params)
    {
            isset($params['id']) && $this->setId($params['id']);
            isset($params['symbol_name']) && $this->setSymbolName($params['symbol_name']);
            isset($params['business_name']) && $this->setBusinessName($params['business_name']);
            isset($params['count']) && $this->setCount($params['count']);
            isset($params['value']) && $this->setValue($params['value']);
            isset($params['turnover']) && $this->setTurnover($params['turnover']);
            isset($params['last_previous_transaction']) && $this->setLastPreviousTransaction($params['last_previous_transaction']);
            isset($params['first_transaction']) && $this->setFirstTransaction($params['first_transaction']);
            isset($params['last_transaction']) && $this->setLastTransaction($params['last_transaction']);
            isset($params['last_transaction_change']) && $this->setLastTransactionChange($params['last_transaction_change']);
            isset($params['last_transaction_percent']) && $this->setLastTransactionPercent($params['last_transaction_percent']);
            isset($params['final_transaction']) && $this->setFinalTransaction($params['final_transaction']);
            isset($params['final_transaction_change']) && $this->setFinalTransactionChange($params['final_transaction_change']);
            isset($params['final_transaction_percent']) && $this->setFinalTransactionPercent($params['final_transaction_percent']);
            isset($params['lowest_transaction']) && $this->setLowestTransaction($params['lowest_transaction']);
            isset($params['highest_transaction']) && $this->setHighestTransaction($params['highest_transaction']);
            isset($params['date']) && $this->setDate($params['date']);
            isset($params['created_at']) && $this->setCreatedAt($params['created_at']);
            isset($params['updated_at']) && $this->setUpdatedAt($params['updated_at']);
            isset($params['deleted_at']) && $this->setDeletedAt($params['deleted_at']);
    }
}
