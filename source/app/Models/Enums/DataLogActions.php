<?php

namespace App\Models\Enums;


class DataLogActions extends Enum
{
    const CREATE = 'create';
    const UPDATE = 'update';
    const DELETE = 'delete';
}
