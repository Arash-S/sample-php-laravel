<?php

namespace App\Models\Enums;

class Enum
{
    public function __construct()
    {
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getList()
    {
        $class = new \ReflectionClass($this);
        return $class->getConstants();
    }
}

