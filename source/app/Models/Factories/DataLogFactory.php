<?php

namespace App\Models\Factories;

use App\Models\Entities\DataLog;
use stdClass;

class DataLogFactory extends Factory
{
    /**
     * @param stdClass $entity
     * @return DataLog
     */
    public function make(stdClass $entity)
    {
        $dataLog = new DataLog();
        $dataLog->setId($entity->id ?? null);
        $dataLog->setUserId($entity->user_id);
        $dataLog->setTable($entity->table);
        $dataLog->setReference($entity->reference);
        $dataLog->setAction($entity->action);
        $dataLog->setData($entity->data);
        $dataLog->setCreatedAt($entity->created_at ?? null);

        return $dataLog;
    }
}
