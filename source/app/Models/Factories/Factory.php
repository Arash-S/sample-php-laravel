<?php

namespace App\Models\Factories;

use stdClass;
use Illuminate\Support\Collection;

abstract class Factory
{
    /**
     * @param stdClass $entity
     * @return mixed
     */
    public abstract function make(stdClass $entity);

    /**
     * @param Collection $entities
     * @return Collection
     */
    public function makeCollection(Collection $entities)
    {
        $result = collect();

        foreach ($entities as $_entity) {
            $result->push($this->make($_entity));
        }

        return $result;
    }

    /**
     * @param array $entities
     * @return Collection
     */
    public function makeArray(array $entities)
    {
        $result = collect();

        foreach ($entities as $_entity) {
            $result->push($this->make((object) $_entity));
        }

        return $result;
    }
}
