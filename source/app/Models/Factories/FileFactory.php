<?php

namespace App\Models\Factories;

use App\Models\Entities\File;
use stdClass;

class FileFactory extends Factory
{
    /**
     * @param stdClass $entity
     * @return File
     */
    public function make(stdClass $entity)
    {
        $file = new File();
        $file->setId($entity->id ?? null);
        $file->setHash($entity->hash);
        $file->setPath($entity->path);
        $file->setCreatedAt($entity->created_at ?? null);

        return $file;
    }
}
