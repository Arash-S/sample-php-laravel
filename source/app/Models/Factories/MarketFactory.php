<?php

namespace App\Models\Factories;

use App\Models\Entities\Market;
use stdClass;

class MarketFactory extends Factory
{
    /**
     * @param stdClass $entity
     * @return Market
     */
    public function make(stdClass $entity)
    {
        $market = new Market();
        $market->setId($entity->id ?? null);
        $market->setSymbolName($entity->symbol_name);
        $market->setBusinessName($entity->business_name);
        $market->setCount($entity->count);
        $market->setValue($entity->value);
        $market->setTurnover($entity->turnover);
        $market->setLastPreviousTransaction($entity->last_previous_transaction);
        $market->setFirstTransaction($entity->first_transaction);
        $market->setLastTransaction($entity->last_transaction);
        $market->setLastTransactionChange($entity->last_transaction_change);
        $market->setLastTransactionPercent($entity->last_transaction_percent);
        $market->setFinalTransaction($entity->final_transaction);
        $market->setFinalTransactionChange($entity->final_transaction_change);
        $market->setFinalTransactionPercent($entity->final_transaction_percent);
        $market->setLowestTransaction($entity->lowest_transaction);
        $market->setHighestTransaction($entity->highest_transaction);
        $market->setDate($entity->date);
        $market->setCreatedAt($entity->created_at ?? null);
        $market->setUpdatedAt($entity->updated_at ?? null);
        $market->setDeletedAt($entity->deleted_at ?? null);

        return $market;
    }
}
