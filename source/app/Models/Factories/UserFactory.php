<?php

namespace App\Models\Factories;

use App\Models\Entities\User;
use stdClass;

class UserFactory extends Factory
{
    /**
     * @param stdClass $entity
     * @return User
     */
    public function make(stdClass $entity)
    {
        $user = new User();
        $user->setId($entity->id ?? null);
        $user->setName($entity->name);
        $user->setEmail($entity->email);
        $user->setEmailVerifiedAt($entity->emaill_verficied_at ?? null);
        $user->setPassword($entity->password);
        $user->setRememberToken($entity->remeber_token ?? null);
        $user->setCreatedAt($entity->created_at ?? null);
        $user->setUpdatedAt($entity->updated_at ?? null);
        $user->setDeletedAt($entity->deleted_at ?? null);

        return $user;
    }
}
