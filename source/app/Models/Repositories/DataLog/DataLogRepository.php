<?php

namespace App\Models\Repositories\DataLog;

use App\Models\Entities\DataLog;

class DataLogRepository implements IDataLogRepository
{
    /** @var MySqlDataLogRepository */
    private $mysqlRepository;
    /** @var RedisDataLogRepository */
    private $redisRepsoitory;

    public function __construct()
    {
        $this->mysqlRepository = new MySqlDataLogRepository();
        $this->redisRepsoitory = new RedisDataLogRepository();
    }

    /**
     * @param DataLog $dataLog
     * @return DataLog
     */
    public function create(DataLog $dataLog)
    {
        $entity = $this->mysqlRepository->create($dataLog);
        $this->redisRepsoitory->clear();

        return $entity;
    }

    public function delete($id)
    {
        $result = $this->mysqlRepository->delete($id);
        if ($result) {
            $this->redisRepsoitory->clear();
        }

        return $result;
    }
}
