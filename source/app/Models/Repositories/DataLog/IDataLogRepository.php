<?php

namespace App\Models\Repositories\DataLog;

use App\Models\Entities\DataLog;

interface IDataLogRepository
{
    /**
     * @param DataLog $dataLog
     * @return DataLog
     */
    public function create(DataLog $dataLog);

    /**
     * @param int $id
     * @return int
     */
    public function delete($id);
}
