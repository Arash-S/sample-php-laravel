<?php

namespace App\Models\Repositories\DataLog;

use App\Models\Entities\DataLog;
use App\Models\Repositories\MySqlRepository;

class MySqlDataLogRepository extends MySqlRepository implements IDataLogRepository
{
    /** @var string */
    protected $table = 'data_log';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var bool */
    protected $softDelete = false;

    /**
     * @param DataLog $dataLog
     * @return DataLog
     */
    public function create(DataLog $dataLog)
    {
        $dataLog->setCreatedAt(new \DateTime());
        $id = $this->newQuery()->insertGetId([
            'user_id' => $dataLog->getUserId(),
            'table' => $dataLog->getTable(),
            'reference' => $dataLog->getReference(),
            'action' => $dataLog->getAction(),
            'data' => $dataLog->getData(),
            'created_at' => $dataLog->getCreatedAt()
        ]);

        $dataLog->setId($id);
        return $dataLog;
    }
}
