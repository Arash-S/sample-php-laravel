<?php

namespace App\Models\Repositories\DataLog;

use App\Models\Repositories\Redis\QueryCacheStrategy;
use App\Models\Repositories\RedisRepository;

class RedisDataLogRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'DataLog';
        parent::__construct();
    }
}
