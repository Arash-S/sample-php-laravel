<?php

namespace App\Models\Repositories\File;

use App\Models\Entities\File;

class FileRepository implements IFileRepository
{
    /** @var MySqlFileRepository */
    private $mysqlRepository;
    /** @var RedisFileRepository */
    private $redisRepository;

    public function __construct()
    {
        $this->mysqlRepository = new MySqlFileRepository();
        $this->redisRepository = new RedisFileRepository();
    }

    /**
     * @param File $file
     * @return File
     */
    public function create(File $file)
    {
        $file = $this->mysqlRepository->create($file);
        $this->redisRepository->clear();
        return $file;
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete($id)
    {
        $result = $this->mysqlRepository->delete($id);
        if ($result) {
            $this->redisRepository->clear();
        }
        return $result;
    }

    public function getOneByHash($hash)
    {
        $cacheKey = $this->redisRepository->makeKey(['hash' => $hash]);
        $entity = $this->redisRepository->get($cacheKey);

        if (is_null($entity)) {
            $entity = $this->mysqlRepository->getOneByHash($hash);
            $this->redisRepository->put($cacheKey, $entity);
        }

        return $entity;
    }
}
