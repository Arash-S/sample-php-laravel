<?php

namespace App\Models\Repositories\File;

use App\Models\Entities\File;

interface IFileRepository
{
    /**
     * @param File $file
     * @return File
     */
    public function create(File $file);

    /**
     * @param int $id
     * @return int
     */
    public function delete($id);
}
