<?php

namespace App\Models\Repositories\File;

use App\Models\Entities\File;
use App\Models\Factories\FileFactory;
use App\Models\Repositories\MySqlRepository;

class MySqlFileRepository extends MySqlRepository implements IFileRepository
{
    /** @var string */
    protected $table = 'files';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var bool */
    protected $softDelete = false;

    /**
     * @param File $file
     * @return File
     */
    public function create(File $file)
    {
        $file->setCreatedAt(new \DateTime());
        $id = $this->newQuery()->insertGetId([
            'hash' => $file->getHash(),
            'path' => $file->getPath(),
            'created_at' => $file->getCreatedAt()
        ]);

        $file->setId($id);
        return $file;
    }

    /**
     * @param string $hash
     * @return File|null
     */
    public function getOneByHash($hash)
    {
        $entity = $this->newQuery()->where('hash', $hash)->first();
        return $entity ? (new FileFactory())->make($entity) : null;
    }
}
