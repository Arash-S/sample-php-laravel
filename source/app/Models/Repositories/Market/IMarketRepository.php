<?php

namespace App\Models\Repositories\Market;

use App\Models\Entities\Market;
use Illuminate\Support\Collection;

interface IMarketRepository
{
    /**
     * @param Market $market
     * @return Market
     */
    public function create(Market $market);

    /**
     * @param Market $market
     * @return int
     */
    public function update(Market $market);

    /**
     * @param int $id
     * @return int
     */
    public function delete($id);

    /**
     * @param int $offset
     * @param int $count
     * @param int|null $total
     * @param array $orders
     * @param array $filters
     * @return Market[]|Collection
     */
    public function search($offset = 0, $count = 0, &$total = null, $orders = null, $filters = null);
}
