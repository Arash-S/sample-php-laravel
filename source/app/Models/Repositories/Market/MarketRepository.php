<?php

namespace App\Models\Repositories\Market;

use App\Models\Entities\Market;
use Illuminate\Support\Collection;

class MarketRepository implements IMarketRepository
{
    /** @var MySqlMarketRepository */
    private $mysqlRepository;
    /** @var RedisMarketRepository */
    private $redisRepository;

    public function __construct()
    {
        $this->mysqlRepository = new MySqlMarketRepository();
        $this->redisRepository = new RedisMarketRepository();
    }

    /**
     * @param Market $market
     * @return Market
     */
    public function create(Market $market)
    {
        $entity = $this->mysqlRepository->create($market);
        $this->redisRepository->clear();
        return $entity;
    }

    /**
     * @param Market $market
     * @return int|void
     */
    public function update(Market $market)
    {
        $result = $this->mysqlRepository->update($market);
        if ($result) {
            $this->redisRepository->clear();
        }

        return $result;
    }

    /**
     * @param Market[]|Collection $markets
     * @return void
     * @throws \Throwable
     */
    public function bulkCreate($markets)
    {
        $this->mysqlRepository->bulkCreate($markets);
        $this->redisRepository->clear();
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete($id)
    {
        $result = $this->mysqlRepository->delete($id);
        if ($result) {
            $this->redisRepository->clear();
        }

        return $result;
    }

    /**
     * @param int $offset
     * @param int $count
     * @param int|null $total
     * @param array $orders
     * @param array $filters
     * @return Market[]|Collection
     */
    public function search($offset = 0, $count = 0, &$total = null, $orders = null, $filters = null)
    {
        $generalCacheKey = $this->redisRepository->makeKey(['offset' => $offset, 'count' => $count, 'filters' => $filters]);
        $totalCacheKey = $this->redisRepository->makeKey(['total' => $generalCacheKey]);

        if ($this->redisRepository->has($generalCacheKey) && $this->redisRepository->has($totalCacheKey)) {
            $total = $this->redisRepository->get($totalCacheKey);
            $entities = $this->redisRepository->get($generalCacheKey);
        } else {
            $entities = $this->mysqlRepository->search($offset, $count, $total, $orders, $filters);
            $this->redisRepository->put($totalCacheKey, $total);
            $this->redisRepository->put($generalCacheKey, $entities);
        }

        return $entities;
    }
}
