<?php

namespace App\Models\Repositories\Market;

use App\Models\Entities\Market;
use App\Models\Factories\MarketFactory;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Collection;
use Illuminate\Database\DatabaseManager;

class MySqlMarketRepository extends MySqlRepository implements IMarketRepository
{
    /** @var string */
    protected $table = 'market';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var bool */
    protected $softDelete = true;

    /**
     * @param Market $market
     * @return Market
     */
    public function create(Market $market)
    {
        $market->setCreatedAt(new \DateTime());
        $id = $this->newQuery()->insertGetId([
            'symbol_name' => $market->getSymbolName(),
            'business_name' => $market->getBusinessName(),
            'count' => $market->getCount(),
            'value' => $market->getValue(),
            'turnover' => $market->getTurnover(),
            'last_previous_transaction' => $market->getLastPreviousTransaction(),
            'first_transaction' => $market->getFirstTransaction(),
            'last_transaction' => $market->getLastTransaction(),
            'last_transaction_change' => $market->getLastTransactionChange(),
            'last_transaction_percent' => $market->getLastTransactionPercent(),
            'final_transaction' => $market->getFinalTransaction(),
            'final_transaction_change' => $market->getFinalTransactionChange(),
            'final_transaction_percent' => $market->getFinalTransactionPercent(),
            'lowest_transaction' => $market->getLowestTransaction(),
            'highest_transaction' => $market->getHighestTransaction(),
            'date' => $market->getDate(),
            'created_at' => $market->getCreatedAt()
        ]);

        $market->setId($id);
        return $market;
    }

    /**
     * @param Market $market
     * @return int
     */
    public function update(Market $market)
    {
        $market->setUpdatedAt(new \DateTime());
        return $this->newQuery()->where($this->primaryKey, $market->getId())->update([
            'symbol_name' => $market->getSymbolName(),
            'business_name' => $market->getBusinessName(),
            'count' => $market->getCount(),
            'value' => $market->getValue(),
            'turnover' => $market->getTurnover(),
            'last_previous_transaction' => $market->getLastPreviousTransaction(),
            'first_transaction' => $market->getFirstTransaction(),
            'last_transaction' => $market->getLastTransaction(),
            'last_transaction_change' => $market->getLastTransactionChange(),
            'last_transaction_percent' => $market->getLastTransactionPercent(),
            'final_transaction' => $market->getFinalTransaction(),
            'final_transaction_change' => $market->getFinalTransactionChange(),
            'final_transaction_percent' => $market->getFinalTransactionPercent(),
            'lowest_transaction' => $market->getLowestTransaction(),
            'highest_transaction' => $market->getHighestTransaction(),
            'updated_at' => $market->getUpdatedAt()
        ]);
    }

    /**
     * @param Market[]|Collection $markets
     * @return void
     * @throws \Throwable
     */
    public function bulkCreate($markets)
    {
        /** @var DatabaseManager $db */
        $db = app('db');
        $db->transaction(function () use ($markets) {
            foreach ($markets as $_market) {
                $this->create($_market);
            }
        });
    }

    /**
     * @param int $offset
     * @param int $count
     * @param int|null $total
     * @param array $orders
     * @param array $filters
     * @return Market[]|Collection
     * @throws \Exception
     */
    public function search($offset = 0, $count = 0, &$total = null, $orders = null, $filters = null)
    {
        $query = $this->newQuery();

        $total = $query->count();
        if ($count) {
            $query->offset($offset);
            $query->limit($count);
        }

        $entities = $query->get();
        return (new MarketFactory())->makeCollection($entities);
    }
}
