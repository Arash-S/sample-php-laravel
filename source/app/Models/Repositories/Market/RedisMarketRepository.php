<?php

namespace App\Models\Repositories\Market;

use App\Models\Repositories\RedisRepository;
use App\Models\Repositories\Redis\QueryCacheStrategy;

class RedisMarketRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'Market';
        parent::__construct();
    }
}
