<?php

namespace App\Models\Repositories;

use Illuminate\Database\Query\Builder;

abstract class MySqlRepository
{
    /** @var string */
    protected $table = '';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var bool */
    protected $softDelete = false;

    /**
     * @return Builder
     */
    public function newQuery()
    {
        return app('db')->table($this->table);
    }

    /**
     * @param mixed $columnValue
     * @param string $columnName
     * @return bool
     */
    public function exists($columnValue, $columnName = null)
    {
        if (is_null($columnName)) {
            $columnName = $this->primaryKey;
        }

        return $this->newQuery()->where($columnName, $columnValue)->exists();
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete($id)
    {
        if ($this->softDelete) {
            $result = $this->newQuery()->where($this->primaryKey, $id)->update(['deleted_at' => new \DateTime()]);
        } else {
            $result = $this->newQuery()->where($this->primaryKey, $id)->delete();
        }

        return $result;
    }
}
