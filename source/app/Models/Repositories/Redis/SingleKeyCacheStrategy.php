<?php

namespace App\Models\Repositories\Redis;

use Illuminate\Support\Collection;

trait SingleKeyCacheStrategy
{
    /** @var string $cacheKey */
    private $cacheKey = '';

    /**
     * @return bool
     */
    public function has()
    {
        return $this->getCache()->has($this->cacheKey);
    }

    /**
     * @param array|Collection $entities
     */
    public function put($entities)
    {
        $this->getCache()->forever($this->cacheKey, $entities);
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->getCache()->get($this->cacheKey);
    }

    /**
     * @return mixed
     */
    public function clear()
    {
        return $this->getCache()->forget($this->cacheKey);
    }
}

