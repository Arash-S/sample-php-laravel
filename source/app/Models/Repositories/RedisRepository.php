<?php

namespace App\Models\Repositories;

use Illuminate\Cache\CacheManager;

abstract class RedisRepository
{
    /** @var CacheManager $cache */
    private $cache;

    public function __construct()
    {
        $this->cache = app('cache');
    }

    /**
     * @return CacheManager
     */
    protected function getCache()
    {
        return $this->cache;
    }
}
