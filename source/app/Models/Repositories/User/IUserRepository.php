<?php

namespace App\Models\Repositories\User;

use App\Models\Entities\User;

interface IUserRepository
{
    /**
     * @param User $user
     * @return User
     */
    public function create(User $user);

    /**
     * @param User $user
     * @return int
     */
    public function update(User $user);

    /**
     * @param int $id
     * @return int
     */
    public function delete($id);
}
