<?php

namespace App\Models\Repositories\User;

use App\Models\Entities\User;
use App\Models\Repositories\MySqlRepository;
use Illuminate\Support\Facades\Hash;

class MySqlUserRepository extends MySqlRepository implements IUserRepository
{
    /** @var string */
    protected $table = 'users';
    /** @var string */
    protected $primaryKey = 'id';
    /** @var bool */
    protected $softDelete = true;

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user)
    {
        $user->setCreatedAt(new \DateTime());
        $id = $this->newQuery()->insertGetId([
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => Hash::make($user->getPassword()),
            'created_at' => $user->getCreatedAt()
        ]);

        $user->setId($id);
        return $user;
    }

    public function update(User $user)
    {
        $user->setUpdatedAt(new \DateTime());
        return $this->newQuery()->where($this->primaryKey, $user->getId())->update([
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => Hash::make($user->getPassword()),
            'updated_at' => $user->getUpdatedAt()
        ]);
    }
}
