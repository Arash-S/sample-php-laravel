<?php

namespace App\Models\Repositories\User;

use App\Models\Repositories\RedisRepository;
use App\Models\Repositories\Redis\QueryCacheStrategy;

class RedisUserRepository extends RedisRepository
{
    use QueryCacheStrategy;

    public function __construct()
    {
        $this->cacheTag = 'users';
        parent::__construct();
    }
}
