<?php

namespace App\Models\Repositories\User;

use App\Models\Entities\User;

class UserRepository implements IUserRepository
{
    /** @var MySqlUserRepository */
    private $mysqlRepository;
    /** @var RedisUserRepository */
    private $redisRepository;

    public function __construct()
    {
        $this->mysqlRepository = new MySqlUserRepository();
        $this->redisRepository = new RedisUserRepository();
    }

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user)
    {
        $entity = $this->mysqlRepository->create($user);
        $this->redisRepository->clear();
        return $entity;
    }

    /**
     * @param User $user
     * @return int
     */
    public function update(User $user)
    {
        $result = $this->mysqlRepository->update($user);
        if ($result) {
            $this->redisRepository->clear();
        }

        return $result;
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete($id)
    {
        $result = $this->mysqlRepository->delete($id);
        if ($result) {
            $this->redisRepository->clear();
        }

        return $result;
    }
}
