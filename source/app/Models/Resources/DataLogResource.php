<?php

namespace App\Models\Resources;

use App\Models\Entities\DataLog;

class DataLogResource extends Resource
{
    /** @var DataLog */
    private $dataLog;

    /**
     * @param DataLog $dataLog
     */
    public function __construct(DataLog $dataLog)
    {
        $this->dataLog = $dataLog;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->dataLog->getId(),
            'user_id' => $this->dataLog->getUserId(),
            'table' => $this->dataLog->getTable(),
            'reference' => $this->dataLog->getReference(),
            'action' => $this->dataLog->getAction(),
            'data' => $this->dataLog->getData(),
            'created_at' => $this->dataLog->getCreatedAt()
        ];
    }
}
