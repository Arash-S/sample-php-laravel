<?php

namespace App\Models\Resources;

use App\Models\Entities\File;

class FileResource extends Resource
{
    /** @var File */
    private $file;

    /**
     * @param File $file
     */
    public function __construct(File $file)
    {
        $this->file = $file;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->file->getId(),
            'hash' => $this->file->getHash(),
            'path' => $this->file->getPath(),
            'created_at' => $this->file->getCreatedAt()
        ];
    }
}
