<?php

namespace App\Models\Resources;

use App\Models\Entities\Market;
use Illuminate\Support\Collection;

class MarketResource extends Resource
{
    /** @var Market[]|Collection */
    private $markets;

    /**
     * @param Market[]|Collection $markets
     */
    public function __construct($markets)
    {
        $this->markets = $markets;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $result = [];

        foreach ($this->markets as $_market) {
            $result[] = [
                'id' => $_market->getId() ?? null,
                'symbol_name' => $_market->getSymbolName(),
                'business_name' => $_market->getBusinessName(),
                'count' => $_market->getCount(),
                'value' => $_market->getValue(),
                'turnover' => $_market->getTurnover(),
                'last_previous_transaction' => $_market->getLastPreviousTransaction(),
                'first_transaction' => $_market->getFirstTransaction(),
                'last_transaction' => $_market->getLastTransaction(),
                'last_transaction_change' => $_market->getLastTransactionChange(),
                'last_transaction_percent' => $_market->getLastTransactionPercent(),
                'final_transaction' => $_market->getFinalTransaction(),
                'final_transaction_change' => $_market->getFinalTransactionChange(),
                'final_transaction_percent' => $_market->getFinalTransactionPercent(),
                'lowest_transaction' => $_market->getLowestTransaction(),
                'highest_transaction' => $_market->getHighestTransaction(),
                'date' => $_market->getDate(),
                'created_at' => $_market->getCreatedAt(),
                'updated_at' => $_market->getUpdatedAt() ?? null,
                'deleted_at' => $_market->getDeletedAt() ?? null,
            ];
        }

        return $result;
    }
}
