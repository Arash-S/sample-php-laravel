<?php

namespace App\Models\Resources;

abstract class Resource
{
    abstract public function toArray();
}
