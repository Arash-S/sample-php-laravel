<?php

namespace App\Models\Resources;

use App\Models\Entities\User;

class UserResource extends Resource
{
    /** @var User */
    private $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'name' => $this->user->getName(),
            'email' => $this->user->getEmail()
        ];
    }
}
