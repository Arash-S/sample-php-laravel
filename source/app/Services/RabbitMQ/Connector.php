<?php

namespace App\Services\RabbitMQ;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPRuntimeException;

class Connector
{
    /** @var AMQPStreamConnection */
    private $connection;
    /** @var AMQPChannel */
    private $channel;
    /** @var string */
    private $queue;

    /**
     * @param string $queue
     */
    public function __construct($queue)
    {
        $config = config('queue.connections.rabbitmq');

        $this->queue = $queue;
        $this->connection = new AMQPStreamConnection($config['host'], $config['port'], $config['user'], $config['password']);
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare($this->queue, false, true, false, false);
    }

    /**
     * @param $message
     * @return AMQPMessage
     */
    public function makeMessage($message)
    {
        return new AMQPMessage($message);
    }

    /**
     * @param $message
     * @return void
     */
    public function basicPublish($message)
    {
        $this->channel->basic_publish($message, '', $this->queue);
    }

    /**
     * @param callable $callback
     * @return void
     */
    public function basicConsume(callable $callback)
    {
        $this->channel->basic_consume($this->queue, '', false, true, false, false, $callback);
    }

    /**
     * @return void
     * @throws \ErrorException
     */
    public function wait()
    {
        try {
            $this->channel->wait();
        } catch (AMQPRuntimeException $e) {}
    }

    /**
     * @return void
     */
    public function close()
    {
        $this->channel->close();
        $this->connection->close();
    }
}
