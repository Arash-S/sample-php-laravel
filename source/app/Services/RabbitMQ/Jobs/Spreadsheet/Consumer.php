<?php

namespace App\Services\RabbitMQ\Jobs\Spreadsheet;

use App\Models\Entities\Market;
use App\Models\Repositories\Market\MarketRepository;
use App\Services\RabbitMQ\Connector;
use App\Services\RabbitMQ\Jobs\Enums\Queues;
use Morilog\Jalali\CalendarUtils;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\Http;

class Consumer
{
    /**
     * @param array $params
     * @return bool
     */
    static private function dataValidation(&$params)
    {
        if (!is_string($params[0]) ||
            !is_string($params[1]) ||
            !is_string($params[15])) {
            return false;
        }

        for ($i = 2; $i < 15; $i++)
            if (!is_int($params[$i])) {
                if (!is_numeric($params[$i])) {
                    return false;
                }

                if (8 == $i || 9 == $i || 11 == $i || 12 == $i) {
                    $params[$i] = (float)$params[$i];
                } else {
                    $params[$i] = (int)$params[$i];
                }
            }

        try {
            $date = str_replace('/', '-', $params[15]);
            $date = CalendarUtils::convertNumbers($date, true);
            $date = CalendarUtils::createCarbonFromFormat("Y-m-d", $date)->format("Y-m-d");
            $params[15] = new \DateTime($date);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $message
     * @return void
     * @throws \Exception
     */
    static public function callback($message)
    {
        $reader = new Xlsx();
        $spreadsheet = $reader->load($message->body);
        $activeSheet = $spreadsheet->getActiveSheet();

        $has_error = 0;
        $data = collect();
        foreach ($activeSheet->toArray() as $_row_key => $_row_value) {
            try {
                if (!self::dataValidation($_row_value)) continue;
                $market = new Market();
                $market->fill([
                    'symbol_name' => $_row_value[0],
                    'business_name' => $_row_value[1],
                    'count' => $_row_value[2],
                    'value' => $_row_value[3],
                    'turnover' => $_row_value[4],
                    'last_previous_transaction' => $_row_value[5],
                    'first_transaction' => $_row_value[6],
                    'last_transaction' => $_row_value[7],
                    'last_transaction_change' => $_row_value[8],
                    'last_transaction_percent' => $_row_value[9],
                    'final_transaction' => $_row_value[10],
                    'final_transaction_change' => $_row_value[11],
                    'final_transaction_percent' => $_row_value[12],
                    'lowest_transaction' => $_row_value[13],
                    'highest_transaction' => $_row_value[14],
                    'date' => $_row_value[15],
                ]);
                $data->push($market);
            } catch (\Exception $e) {
                $has_error++;
            }
        }

        if (!$data->isEmpty()) {
            (new MarketRepository())->bulkCreate($data);
        }

        Http::post(
            "https://paraf.app/quiz/job/log.php",
            [
                "data" => ["success" => $data->count(), "error" => $has_error],
                "message" => "Success",
                "datetime" => (new \DateTime())->format("Y-m-d")
            ]
        );
    }

    /**
     * @return void
     * @throws \ErrorException
     */
    static public function register()
    {
        $queue = new Connector(Queues::SPREADSHEET);
        $queue->basicConsume([Consumer::class, 'callback']);
        $queue->wait();
        $queue->close();
    }
}
