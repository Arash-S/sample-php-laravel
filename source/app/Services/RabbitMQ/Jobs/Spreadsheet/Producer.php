<?php

namespace App\Services\RabbitMQ\Jobs\Spreadsheet;

use App\Services\RabbitMQ\Connector;
use App\Services\RabbitMQ\Jobs\Enums\Queues;

class Producer
{
    /**
     * @param $path
     * @return void
     */
    static public function make($path)
    {
        $queue = new Connector(Queues::SPREADSHEET);
        $message = $queue->makeMessage($path);
        $queue->basicPublish($message);
        $queue->close();
    }
}
