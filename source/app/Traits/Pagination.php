<?php

namespace App\Traits;

trait Pagination
{
    /**
     * @param null $page
     * @param null $count
     * @param null $offset
     * @param null $limit_counting
     * @return object
     */
    public function getPagination($page = null, $count = null, $offset = null, $limit_counting = null)
    {
        $result = [
            'page' => is_null($page) ? $offset * $count : $page,
            'count' => $count,
            'offset' => empty($offset) ? 0 : $offset
        ];

        if ($count && $limit_counting && $result['count'] > $limit_counting) {
            $count = $limit_counting;
        }

        if ($page) {
            $result['page'] = $page ?: 1;
            $result['count'] = $count ?: 16;
            $result['offset'] = ($page - 1) * $count;
        }

        return (object) $result;
    }
}
