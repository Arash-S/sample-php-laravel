<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market', function (Blueprint $table) {
            $table->id();
            $table->string('symbol_name', 254);
            $table->string('business_name', 512);
            $table->unsignedInteger('count');
            $table->unsignedBigInteger('value');
            $table->unsignedBigInteger('turnover');
            $table->unsignedBigInteger('last_previous_transaction');
            $table->unsignedBigInteger('first_transaction');
            $table->unsignedBigInteger('last_transaction');
            $table->double('last_transaction_change');
            $table->float('last_transaction_percent');
            $table->unsignedBigInteger('final_transaction');
            $table->double('final_transaction_change');
            $table->float('final_transaction_percent');
            $table->unsignedBigInteger('lowest_transaction');
            $table->unsignedBigInteger('highest_transaction');
            $table->timestamp('date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market');
    }
};
