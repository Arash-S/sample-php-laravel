<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    <title>{{$title ?? ""}}</title>
</head>
<body class="bg-gray-200 font-serif">
<nav class="p-6 bg-white flex justify-between mb-6">
    <ul class="flex items-center">
        @guest
            <li>
                <a href="{{ route('sign_in') }}" class="p-3">sign in</a>
                <a href="{{ route('sign_up') }}" class="p-3">sign up</a>
            </li>
        @endguest
        @auth
            <li>
                <a href="{{ route('dashboard_index') }}" class="p-3">Dashboard</a>
                <a href="{{ route('market_index') }}" class="p-3">Market</a>
            </li>
        @endauth
    </ul>
</nav>
@yield('content')
</body>
</html>
