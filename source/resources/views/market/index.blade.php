@extends('layout.base')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            <form action="{{ route('file_upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-1 flex">
                    <label for="file" class="sr-only">file</label>
                    <input type="file" name="file" id="file" class="bg-gray-100 border-2 w-5/6 px-4 py-2 rounded-lg">
                    <button type="submit" class="bg-blue-500 text-white px-4 py-2 w-1/6 font-medium rounded-lg">upload
                    </button>
                </div>
                <div class="mb-4 flex">
                    @error('file')
                    <div class="text-red-500 mt-2 text sm">{{ $message }}</div>
                    @enderror
                </div>
            </form>

            <table id="market-info" class="table pt-4 mb-4 flex w-full" style="width: 100% !important;">
                <thead>
                <tr>
                    <th>id</th>
                    <th>symbol</th>
                    <th>business</th>
                    <th>count</th>
                    <th>value</th>
                    <th>turnover</th>
                    <th>previous</th>
                    <th>first</th>
                    <th>last</th>
                    <th>last change</th>
                    <th>last percent</th>
                    <th>final</th>
                    <th>final change</th>
                    <th>final percent</th>
                    <th>lowest</th>
                    <th>highest</th>
                    <th>date</th>
                </tr>
                </thead>
                <tbody class="flex-row">
                </tbody>
            </table>

        </div>
    </div>


    <script>
        $(document).ready(function () {
            dataTable();
        });

        function dataTable() {
            let dataTable = $('#market-info').DataTable({
                serverSide: true,
                ajax: {
                    processing: true,
                    url: "{{ route('api_market_list') }}",
                    type: 'GET',
                    datatype: 'json',
                    data: function (data) {
                        return data;
                    },
                    dataFilter: function (data) {
                        let json = JSON.parse(data);
                        json.recordsTotal = json.meta.total;
                        json.recordsFiltered = json.meta.total;
                        return JSON.stringify(json);
                    },
                },
                columns: [
                    {
                        data: 'id',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'symbol_name',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'business_name',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'count',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'value',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'turnover',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'last_previous_transaction',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'first_transaction',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'last_transaction',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'last_transaction_change',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'last_transaction_percent',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'final_transaction',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'final_transaction_change',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'final_transaction_percent',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'lowest_transaction',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'highest_transaction',
                        className: 'dt-center flex-1',
                        orderable: false
                    },
                    {
                        data: 'date',
                        className: 'dt-center flex-1',
                        orderable: false
                    }
                ]
            });

            $('#DataTable_filter input').unbind();
            $('#DataTable_filter input').bind('keyup', function (e) {
                if (13 == e.keyCode) {
                    dataTable.search(this.value).draw();
                }
            });

            $("#market-info_filter").hide();
        }
    </script>
@endsection
