<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest', 'prefix' => '/'], function () {
    Route::get('', [\App\Http\Controllers\Web\HomeController::class, 'index'])->name('index');
    Route::get('sign-in/', [\App\Http\Controllers\Web\AuthController::class, 'signIn'])->name('sign_in');
    Route::get('sign-up/', [\App\Http\Controllers\Web\AuthController::class, 'signUp'])->name('sign_up');
    Route::post('sign-up/', [\App\Http\Controllers\Web\AuthController::class, 'register'])->name('register');
    Route::post('sign-in/', [\App\Http\Controllers\Web\AuthController::class, 'login'])->name('login');
});

Route::group(['middleware' => 'auth', 'prefix' => '/'], function () {
    Route::get('dashboard', [\App\Http\Controllers\Web\DashboardController::class, 'index'])->name('dashboard_index');

    Route::prefix('market/')->group(function () {
        Route::get('', [\App\Http\Controllers\Web\MarketController::class, 'index'])->name('market_index');
    });

    Route::prefix('file/')->group(function () {
        Route::post('upload', [\App\Http\Controllers\Web\FileController::class, 'upload'])->name('file_upload');
    });
});


Route::group(['middleware' => 'auth', 'prefix' => 'api/'], function () {
    Route::prefix('v1/')->group(function () {
        Route::prefix('market/')->group(function () {
            Route::get('list/', [\App\Http\Controllers\API\V1\MarketController::class, 'list'])->name('api_market_list');
        });
    });
});
